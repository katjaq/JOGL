# How to use the stack

## Clone the repository

First clone the repository `git clone https://gitlab.com/JOGL/JOGL.git` or `git clone git@gitlab.com:JOGL/JOGL.git` if you have a your ssh git setup.

You will need to init the submodules with `git submodule update --init --recursive`

## Update the latest code from both code stack
This will update both the frontend and backend which are git submodules. It will pull from the master branch FYI

`git submodule update --recursive --remote`

## Build the docker images
Next we need to build the docker images for the stack, this needs to be repeated everytime you make a change in the code and want to test it in the docker environment.

`docker-compose build`

This will create 3 images `frontend` `backend` `sidekiq` which we will then start.

## Setting up your environment variables

You will need to set the following environment varialbles in the reminal you will use for the **docker-compose** so that the app can function:
- `ALGOLIA_API_TOKEN`
- `ALGOLIA_APP_ID`

## Backend DB migration and seed
In order for the backend to work we need to migrate the database. It's probably a good idea to do this everytime you pull some new code from the backend. Also we need to make sure the interests are seeded in the database !

- `docker-compose run backend rails db:create`
- `docker-compose run backend rails db:migrate`
- `docker-compose run backend rails db:seed:interests`

## Start the docker stack
docker-compose will run the images we just created along with the necessary databases and maildev.

`docker-compose up -d`

## backend and frontend package update
Normally a simple restart should do the trick

- backend: `docker-compose restart backend sidekiq`
- frontend: `docker-compose restart frontend`

## What's where
You should find the different elements on those ports:
- backend: `http://localhost:3001`
- frontend: `http://localhost:3000`
